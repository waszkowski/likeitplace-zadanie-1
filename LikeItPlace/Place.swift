//
//  Place.swift
//  LikeItPlace
//
//  Created by Wachu I on 12.01.2018.
//  Copyright © 2018 Marek Waszkowski. All rights reserved.
//

import Foundation
import UIKit
import os.log

class Place: NSObject, NSCoding{
  
    //MARK: Properties
    var name: String
    var description_place: String
    var photo: UIImage?
    
    init?(name:String, description:String, photo:UIImage?){
        
        guard !name.isEmpty || !description.isEmpty else {
            return nil
        }
        
        self.name = name
        self.description_place = description
        self.photo = photo
    }
    
    //MARK: Archiving Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("places")
    
    //MARK: Types
    struct PropertyKey {
        static let name = "name"
        static let description_place = "descriptio"
        static let photo = "photo"
    }
    
    //MARK: NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: PropertyKey.name)
        aCoder.encode(photo, forKey: PropertyKey.photo)
        aCoder.encode(description_place, forKey: PropertyKey.description_place)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        guard let name = aDecoder.decodeObject(forKey: PropertyKey.name) as? String else {
            os_log("Unable to decode the name for a Place object.", log: OSLog.default, type: .debug)
            return nil
        }
        
        guard let description_place = aDecoder.decodeObject(forKey: PropertyKey.description_place) as? String else {
            os_log("Unable to decode the description for a Place object.", log: OSLog.default, type: .debug)
            return nil
        }
        
        let photo = aDecoder.decodeObject(forKey: PropertyKey.photo) as? UIImage
        
        self.init(name: name, description: description_place, photo: photo)
        
    }
    
}
