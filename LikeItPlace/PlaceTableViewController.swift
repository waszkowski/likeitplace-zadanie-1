//
//  PlaceTableViewController.swift
//  LikeItPlace
//
//  Created by Wachu I on 12.01.2018.
//  Copyright © 2018 Marek Waszkowski. All rights reserved.
//

import UIKit
import os.log

class PlaceTableViewController: UITableViewController {

    //MARK: Properties
    var places = [Place]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = editButtonItem
        //navigationItem.leftBarButtonItem?.title = "Edycja"

        if let savedPlaces = loadPlaces() {
            places += savedPlaces
        }
        else {
            loadSamplePlace()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "PlaceTableViewCell"

        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? PlaceTableViewCell  else {
            fatalError("The dequeued cell is not an instance of PlaceTableViewCell.")
        }
        
        let place = places[indexPath.row]
        
        cell.nameLabel.text = place.name
        cell.photoImageView.image = place.photo
        cell.descriptionTextView.text = place.description_place

        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        self.tableView.isEditing = true
        
        if editingStyle == .delete {
            places.remove(at: indexPath.row)
            savePlaces()
            tableView.deleteRows(at: [indexPath], with: .fade)
            self.tableView.isEditing = false
        } else if editingStyle == .insert {

        }
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObject = self.places[sourceIndexPath.row]
        places.remove(at: sourceIndexPath.row)
        places.insert(movedObject, at: destinationIndexPath.row)
        savePlaces()
        //NSLog("%@", "\(sourceIndexPath.row) => \(destinationIndexPath.row) \(places)")
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
//    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
//        return .none
//    }
//    
//    override func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
//        return false
//    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        super.prepare(for: segue, sender: sender)

        switch(segue.identifier ?? "") {

        case "AddItem":
            os_log("Adding a new place.", log: OSLog.default, type: .debug)

        case "ShowDetail":
            guard let placeDetailViewController = segue.destination as? PlaceViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }

            guard let selectedPlaceCell = sender as? PlaceTableViewCell else {
                fatalError("Unexpected sender: \(sender)")
            }

            guard let indexPath = tableView.indexPath(for: selectedPlaceCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }

            let selectedPlace = places[indexPath.row]
            placeDetailViewController.place = selectedPlace

        default:
            fatalError("Unexpected Segue Identifier; \(segue.identifier)")
        }
    }

    
    //MARK: Action
    @IBAction func unwindToMealList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? PlaceViewController, let place = sourceViewController.place {
            
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                places[selectedIndexPath.row] = place
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            }
            else {
                savePlaces()
                let newIndexPath = IndexPath(row: places.count, section: 0)
                
                places.append(place)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
            savePlaces()
        }
    }
    
    //MARK: Private Methods
    private func loadSamplePlace(){
        let photo1 = UIImage(named: "place1")
        let photo2 = UIImage(named: "place2")
        let photo3 = UIImage(named: "place3")
        
        let description_place1 = "Jeden z najpiękniejszych kampusów w Polsce. "
        
        let description_place2 = "Piękny poniemiecki dworzec łączacy nowoczesność z budownictwem przedwojennym."
        
        let description_place3 = "Razem z Gdańskiem najpiękniejsze miejsca tego typu. Miejsce spotkań wrocławian."

        guard let place1 = Place(name: "Politechnika Wrocławska", description: description_place1, photo: photo1) else {
            fatalError("Unable to instantiate place1")
        }
        
        guard let place2 = Place(name: "Dworzec Główny PKP", description: description_place2, photo: photo2) else {
            fatalError("Unable to instantiate place2")
        }
        
        guard let place3 = Place(name: "Rynek Wrocławski", description: description_place3, photo: photo3) else {
            fatalError("Unable to instantiate place3")
        }
        
        places += [place1, place2, place3]
        
    }
    
    
    private func savePlaces(){
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(places, toFile: Place.ArchiveURL.path)
        if isSuccessfulSave {
            os_log("Places successfully saved.", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save places...", log: OSLog.default, type: .error)
        }
    }
    
    private func loadPlaces() -> [Place]?  {
        return NSKeyedUnarchiver.unarchiveObject(withFile: Place.ArchiveURL.path) as? [Place]
    }

}
