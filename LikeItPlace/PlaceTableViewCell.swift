//
//  PlaceTableViewCell.swift
//  LikeItPlace
//
//  Created by Wachu I on 12.01.2018.
//  Copyright © 2018 Marek Waszkowski. All rights reserved.
//

import UIKit

class PlaceTableViewCell: UITableViewCell {

    //MARK: Properties
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
